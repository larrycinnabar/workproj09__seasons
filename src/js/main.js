$(function() {

    var App = (function(){

        return {
            init : function() {
                DummyModule.init();
                MenuBehaviour.init();
                FlatsChoose.init();
                Gallery.init();
                SimpleSlider.init();
                Yamap.init();
                YamapInfrastructure.init();
                MainSlider.init();
                SimpleSelect.init();
                TouchEvents.init();
                Popups.init();
                ClickableTables.init();
                Fady.init();
                Flatbar.init();
                FlatPlans.init();

                SmoothLoading.init();

                // Forms validation and handling
                FormCallback.init();
                FormSend.init();
                FormRequest.init();
            }
        }
    })()

    /**
     * Dummy Module Example
     */
    ,MenuBehaviour = (function(){
        return {
            init : function() {
                var $btn = $(".header__navigation");
                var $nav = $("#nav");
                var $overlay = $(".nav__overlay");
                var $header = $("#header");

                if ( !$btn.length ) return false;

                $btn.on('click', () => {
                    $nav.toggleClass('nav--active');
                    $btn.toggleClass('header__navigation--toggled');
                    $overlay.toggleClass('nav__overlay--active');
                    $header.toggleClass('nav-opened');
                    
                    return false;
                });
            }
        }
    })()

    ,FlatsChoose = (function(){
        return {
            init : function() {
                var $btn = $(".flats-menu__caller");
                var $menu = $btn.parents('.flats-choose__menu');
                var $overlay = $(".flats-choose__overlay");
                var $clear_btn = $("#clear_btn");
                var class2toggle = 'flats-choose__menu--active';

                var ww  = ( $(window).width() < window.screen.width ) 
                        ? $(window).width() 
                        : window.screen.width;

                if ( ww > 768 ) {
                    $menu.height( $(".layout-main").height()  + 40);
                } else {
                    $menu.height( $(".layout-main").height() + $(".layout-footer").height());
                }

                if ( !$btn.length ) return false;

                $btn.on('click', () => {
                    $menu.toggleClass(class2toggle);
                    // if ( $menu.hasClass(class2toggle) ) {
                    //     $menu.height( $(".layout-main").height() + $(".layout-footer").height() + 40);
                    // }

                    $overlay.toggleClass('flats-choose__overlay--active');
                    return false;
                });

                $clear_btn.on('click', () => {
                    $(".flats-menu__select").each( (i,e) => {
                        $(e).prop('selectedIndex',0)
                            .simpleselect("refreshContents");
                    });
                    return false;
                });
            }
        }
    })()

    ,Gallery = (function(){
        return {
            init : function() {
                if ( !$('.gallery__list').length ) return false;

                $('.gallery__list').slick({
                  dots: true,
                  infinite: true,
                  speed: 500,
                  cssEase: 'linear',
                  slide : '.gallery__image',
                  prevArrow : $('.gallery__control--prev'),
                  nextArrow : $('.gallery__control--next')
                });
            }
        }
    })()

    ,SimpleSlider = (function(){
        return {
            init : function() {
                if ( !$('.simple-slider').length ) return false;

                $('.simple-slider').slick({
                  dots: false,
                  infinite: true,
                  speed: 500,
                  cssEase: 'linear',
                  slide : 'img',
                  arrows : false
                });
            }
        }
    })()

    ,MainSlider = (function(){
        return {
            init : function() {
                if ( !$('.main-slider__slides').length ) return false;

                $('.main-slider__slides').slick({
                    dots: true,
                    infinite: true,
                    speed: 500,
                    cssEase: 'linear',
                    slide : 'div',
                    arrows : false,
                    autoplay : true,
                    autoplaySpeed : 4000
                });
            }
        }
    })()

    ,SimpleSelect = (function(){
        return {
            init : function() {
                if ( $('.flat-search__select').length ) {
                    $('.flat-search__select').simpleselect();
                }

                if ( $('.flats-menu__select').length ) {
                    $(".flats-menu__select").simpleselect();
                }
            }
        }
    })()

    ,SmoothLoading = ( () => {
        window.RATIO_SET = 0;

        var SL = {};
        SL.checkFlag = (cb) => {
            if(RATIO_SET == 0) {
               window.setTimeout(() => { SL.checkFlag(cb) }, 30);
            } else {
               cb();
            }
        };

        SL.init = (mode) =>  {
            var $whiteoverlay = $("#white_overlay");

            if ( mode ) {
                $whiteoverlay.hide();
                return false;
            }
            
            SL.checkFlag(() => {
                $whiteoverlay.fadeOut();
            })
        };

        return SL;
    })()

    ,TouchEvents = (function(){
        return {
            init : function() {

                var $flatsMenuCaller = $("#flats-menu__caller");
                var $flatsMenu = $(".flats-choose__menu");

                if ( $flatsMenuCaller.length ) {
                    var mc = new Hammer.Manager( $flatsMenuCaller[0], {
                        recognizers: [
                            [Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL}],
                        ]
                    });

                    mc.on('swipe',function(e){
                        if ( $flatsMenu.hasClass('flats-choose__menu--active') ) {
                            if ( e.offsetDirection == Hammer.DIRECTION_LEFT ) {
                                $flatsMenuCaller.trigger('click');
                                return false;
                            }
                        } else {
                            if ( e.offsetDirection == Hammer.DIRECTION_RIGHT ) {
                                $flatsMenuCaller.trigger('click');
                                return false;
                            }
                        }
                    });

                    var mc2 = new Hammer.Manager( $flatsMenu[0], {
                        recognizers: [
                            [Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL }],
                        ]
                    });

                    mc2.on('swipe', function(e){
                        if ( $flatsMenu.hasClass('flats-choose__menu--active') ) {
                            if ( e.offsetDirection == Hammer.DIRECTION_LEFT ) {
                                $flatsMenuCaller.trigger('click');
                                return false;
                            }     
                        }
                    });
                }

                var $nav = $("#nav");
                var $navCaller = $(".header__navigation");

                var mc3 = new Hammer.Manager( $nav[0], {
                    recognizers: [
                        [Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL }],
                    ]
                });

                mc3.on('swipe', function(e){
                    if ( $nav.hasClass('nav--active') ) {
                        if ( e.offsetDirection == Hammer.DIRECTION_LEFT ) {
                            $navCaller.trigger('click');
                            return false;
                        }     
                    }
                });

                // Flat page (swipes between flat pages)
                var $go_upper_link_prev = $(".flat__go--prev");

                if ( $go_upper_link_prev.length ) {
                    var mc4 = new Hammer.Manager( $go_upper_link_prev[0], {
                        recognizers: [
                            [Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL }],
                        ]
                    });
                    mc4.on('swipe', function(e){
                        if ( e.offsetDirection == Hammer.DIRECTION_RIGHT ) {
                            document.location.href = $go_upper_link_prev.attr('href');
                            return false;
                        }     
                    });
                }


                var $go_upper_link_next= $(".flat__go--next");

                if ( $go_upper_link_next.length ) {
                    var mc5 = new Hammer.Manager( $go_upper_link_next[0], {
                        recognizers: [
                            [Hammer.Swipe,{ direction: Hammer.DIRECTION_HORIZONTAL }],
                        ]
                    });
                    mc5.on('swipe', function(e){
                        if ( e.offsetDirection == Hammer.DIRECTION_LEFT ) {
                            document.location.href = $go_upper_link_next.attr('href');
                            return false;
                        }     
                    });
                }

            }
        }
    })()

    ,Yamap = (function(){
        return {
            init : function() {
                if ( !$("#map").length ) return false;

                var map;
                ymaps.ready( () => {
                    var x = 44.891644;
                    var y = 37.315719;

                    map = new ymaps.Map("map", {
                        center: [x, y], 
                        zoom: 14,
                        controls : []
                    });

                    var myPlacemark = new ymaps.Placemark([x,y], {}, {
                        iconLayout: 'default#image',
                        iconImageHref: '/build/images/placemark.png',
                        iconImageSize: [80, 81],
                        iconImageOffset: [0, 0]
                    });

                    map.geoObjects.add(myPlacemark);
                });
            }
        }
    })()

    ,YamapInfrastructure = (function(){
        return {
            init : function() {
                if ( !$("#infrastructure_map").length ) return false;

                var map;
                ymaps.ready( () => {
                    var x = 44.894229;
                    var y = 37.326608;

                    // Создание метки: точка с надписью
                    var dotLayoutCreator = function(title) {
                        return ymaps.templateLayoutFactory.createClass(
                            '<div class="placemark_layout_container"><div class="dot_layout"></div><div class="yamap_title">'
                            +title+
                            '</div></div>'
                        );
                    }

                    // Создание метки: cтрелка с надписью
                    var arrowLayoutCreator = function(title) {
                        return ymaps.templateLayoutFactory.createClass(
                            '<div class="placemark_layout_container"><div class="arrow_layout"></div><div class="yamap_title">'
                            +title+
                            '</div></div>'
                        );
                    }

                    // Создание метки: большая иконка с надписью
                    var seasonsLayoutCreator = function(title) {
                        return ymaps.templateLayoutFactory.createClass(
                            '<div class="placemark_layout_container"><div class="seasons_layout"></div>'
                            +'<div class="yamap_title yamap_title--large">'
                            +title+
                            '</div></div>'
                        );
                    }

                    // Create map
                    map = new ymaps.Map("infrastructure_map", {
                        center   : [44.901592,37.316232], 
                        zoom     : 14,
                        controls : [],
                        type     : 'yandex#satellite'
                    });

                    // Declaare points
                    var PointsCoords = [
                        [44.897742,37.300951],
                        [44.895697,37.309302],
                        [44.890131,37.311184],
                        [44.899267,37.319116],
                        [44.899997,37.332929],
                        [44.887392,37.326018]
                    ];
                    var ArrowsCoords = [
                        [44.908001,37.330479],
                        [44.916540,37.327389]
                    ];
                    var titleOfPointsCoords = [
                        'Центральный пляж',
                        'Администрация города',
                        'Городская больница',
                        'Парк им. 30-летия<br>Победы',
                        'Рынок<br>«Восточный»',
                        'ТРК<br>«Красная площадь»'
                    ];
                    var titleOfArrowsCoords = [
                        'Вокзалы', 'Аэропорты'
                    ]

                    // Main placemark
                    map.geoObjects.add(
                        new ymaps.Placemark([x,y], {}, {
                            iconLayout: seasonsLayoutCreator('Жилой коплекс<br>«Времена года»')
                        })
                    );

                    for (var i = 0, l = PointsCoords.length; i < l; i++) {
                        map.geoObjects.add(
                            new ymaps.Placemark(PointsCoords[i], {}, {
                                iconLayout: dotLayoutCreator(titleOfPointsCoords[i])
                            })
                        ) 
                    }
                    for (var i = 0, l = ArrowsCoords.length; i < l; i++) {
                        map.geoObjects.add(
                            new ymaps.Placemark(ArrowsCoords[i], {}, {
                                iconLayout: arrowLayoutCreator(titleOfArrowsCoords[i])
                            })
                        ) 
                    }
                    
                });
            }
        }
    })()

    ,Popups = (function(){
        var $popup_request = $(".flat__popup-request.popup-request");
        var $btn_request = $("#request_btn");
        var $popup_send = $(".flat__popup-send.popup-send");
        var $btn_send = $("#send_btn");
        var $popup_callback = $(".flat__popup-callback.popup-callback");
        var $btn_callback = $("#callback_btn");

        var $popups = $(".popup-general");
        var $overlays = $(".popup-overlay-general");

        if ( $("#callback_phone").length ) $("#callback_phone").mask("+7 (999) 999-99-99");
        if ( $("#request_phone").length ) $("#request_phone").mask("+7 (999) 999-99-99");

        return {
            init : () => {
                $(".popup__close").on("click", (ev) => {
                    $(ev.target).parents('.popup-general').toggleClass('active');
                    $('.page').css({ 'overflow': 'auto'});

                    return false;
                });

                $btn_request.on('click', () => {
                    $popup_request.toggleClass('active');
                    $('.page').css({'position' : 'static', 'overflow': 'hidden'});
                    $popup_request.find("input").first().focus();
                    
                    return false;
                });


                $btn_send.on('click', () => {
                    $popup_send.toggleClass('active');
                    $('.page').css({'position' : 'static', 'overflow': 'hidden'});
                    $popup_send.find("input").first().focus();
                    
                    return false;
                });

            
                $btn_callback.on('click', () => {
                    $popup_callback.toggleClass('active');
                    $('.page').css({'position' : 'static', 'overflow': 'hidden'});
                    $popup_callback.find("input").first().focus();
                    
                    return false;
                });


                $overlays.on("click", () => {
                    $popups.removeClass('active');
                    $('.page').css({'overflow': 'auto'});
                });

            }
        }
    })()

    /**
     * ClickableTables
     */
    ,ClickableTables = (function(){
        return {
            init : () => {
                $("tr[data-href]").on('click', (e) => {
                    window.document.location = $(e.currentTarget).attr("data-href");
                });
            }
        }
    })()


    ,FormCallback = (function(){
        return {
            init : function() {
                var $form = $("#form_callback");
                if ( !$form.length ) return false;

                $form.validate({
                    messages: {
                        surname: "Пожалуйста, укажите Ваше имя",
                        phone: "Пожалуйста, укажите номер телефона"
                    },
                    submitHandler: function(form) {
                        // сюда процесс сабмита вставляем
                        // $(form).ajaxSubmit();
                        return false;
                    }
                });
            }
        }
    })()

    ,FormSend = (function(){
        return {
            init : function() {
                var $form = $("#form_send");
                if ( !$form.length ) return false;

                $form.validate({
                    messages: {
                        email: {
                            required: "Пожалуйста, укажите адрес Вашей электронной почты",
                            email : "Пожалуйста, введите корректный адрес электронной почты"
                        }
                    },
                    submitHandler: function(form) {
                        // сюда процесс сабмита вставляем
                        // $(form).ajaxSubmit();
                        return false;
                    }
                });
            }
        }
    })()

    ,FormRequest = (function(){
        return {
            init : function() {
                var $form = $("#form_request");
                if ( !$form.length ) return false;

                $form.validate({
                    messages: {
                        surname: "Пожалуйста, укажите Ваше имя",
                        phone: "Пожалуйста, укажите номер телефона",
                        email: {
                            required: "Пожалуйста, укажите адрес Вашей электронной почты",
                            email : "Пожалуйста, введите корректный адрес электронной почты"
                        }
                    },
                    submitHandler: function(form) {
                        // сюда процесс сабмита вставляем
                        // $(form).ajaxSubmit();
                        return false;
                    }
                });
            }
        }
    })()

    /**
     * Fady
     */
    ,Fady = (function(){
        return {
            init : function() {
                var $F = $("#fady");

                if ( !$F.length ) return false;

                var sTransitionEndEvt = 'transitionend webkitTransitionEnd oTransitionEnd';
                $F.addClass('disabled').on(sTransitionEndEvt, function(){
                    $F.remove();
                });

                // на случай если transition не была запущена вовсе
                window.setTimeout(function(){
                    if ( $F.length ) $F.remove();
                }, 400);
            }
        }
    })()

    /**
     * Dummy Module Example
     */
    ,Flatbar = (function(){
        return {
            init : () => {
                var $fca = $("#flatbar__caller");
                var $fcl = $("#flatbar__close");

                if (!$fca.length) return false;

                $fca.on('click', (e) => {
                    $(e.currentTarget).parents('.flatbar').toggleClass('flatbar--closed');
                    return false;
                });

                $fcl.on('click', (e) => {
                    $(e.currentTarget).parents('.flatbar').addClass('flatbar--closed');
                    return false;
                });
            }
        }
    })()

    /**
     * Dummy Module Example
     */
    ,FlatPlans = (function(){
        return {
            init : function() {
                if ( !$(".flat__scheme-button").length ) return false;
                var $scheme = $('.flat__scheme');

                $(".flat__scheme-button").on('click', function(){
                    if ( $(this).hasClass('flat__scheme-button--plan') ) {
                        $scheme.removeClass('flat__scheme--visualization')
                                           .addClass('flat__scheme--plan');
                    } 
                    if ( $(this).hasClass('flat__scheme-button--visualization') ) {
                        $scheme.addClass('flat__scheme--visualization')
                                           .removeClass('flat__scheme--plan');
                    } 
                    $('.flat__scheme-button').removeClass('flat__scheme-button--active');
                    $(this).addClass('flat__scheme-button--active');
                    
                    return false;
                });
            }
        }
    })()

    /**
     * Dummy Module Example
     */
    ,DummyModule = (function(){
        return {
            init : function() {
                // do something
            }
        }
    })()

    ;App.init();

});
